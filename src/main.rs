extern crate mime;
extern crate actix_web;

#[macro_use]
extern crate log;
extern crate log4rs;
extern crate rand;

use std::iter;
use rand::{Rng, thread_rng};
use rand::distributions::Alphanumeric;


use actix_web::{App, HttpRequest, Result, http::Method, server, Responder};
use actix_web::fs::{StaticFileConfig, NamedFile};
use actix_web::http::header::DispositionType;

use std::path::PathBuf;

#[derive(Default)]
struct MyConfig;

impl StaticFileConfig for MyConfig {
    fn content_disposition_map(typ: mime::Name) -> DispositionType {
        DispositionType::Inline //change to Attachment for downloads
    }
}

fn index(req: &HttpRequest) -> Result<NamedFile<MyConfig>> {
    let mut path = PathBuf::new();
    path.push("./public/");
    let _path : PathBuf = req.match_info().query("tail")?;
    path.push(_path.as_path());
    Ok(NamedFile::open_with_config(path, MyConfig)?)
}

fn main() {
    log4rs::init_file("./config/log4rs.yaml", Default::default()).unwrap();
    error!("testing!!");
    server::new(||{
        App::new()
            .resource(r"/public/{tail:.*}", |r| r.method(Method::GET).f(index))
            .resource(r"/api/v0/login", |r| r.method(Method::POST).f(login))
    })
    .bind("127.0.0.1:8081")
    .unwrap()
    .run();
}

fn login(req: &HttpRequest) -> impl Responder{




    println!("LOGINING IN!");
    // format!("test")
    rand_string()

}

fn rand_string() -> String {
    let mut rng = thread_rng();
    iter::repeat(())
        .map(|()| rng.sample(Alphanumeric))
        .take(7)
        .collect()
}

