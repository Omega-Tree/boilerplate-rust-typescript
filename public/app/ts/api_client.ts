
export class APIClient {
    token?: string;
    use_auth: boolean;
    host_url: string;
    host_port?: number;
    full_host: string;
    constructor(host_url: string, host_port?: number, use_auth = false) {
        this.host_url = host_url;
        this.host_port = host_port;
        this.use_auth = use_auth;

        this.full_host = (host_port === undefined) ? host_url : host_url + ":" + host_port;
    }

    async login(username: string, password: string) {
        let result = await this.post("/api/v0/login", { username: username, password: password });
        console.log('result:' + JSON.stringify(result, null, 2));
    }

    async post(endpoint: string, data: any) {
        let full_url = this.full_host + endpoint;
        let response = await fetch(full_url,
            {

                method: "POST", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, cors, *same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    "Content-Type": "application/json",
                    // "Content-Type": "application/x-www-form-urlencoded",
                },
                redirect: "follow", // manual, *follow, error
                referrer: "no-referrer", // no-referrer, *client
                body: JSON.stringify(data), // body data type must match "Content-Type" header

            });

        let response_json = await response.json();
        if (response_json.error_code) {
            if (response_json.error_msg) {
                console.error(`Could not login. Failed to login with the following error msg: "${response_json.err_msg}"`);
            } else {
                console.error(`Could not login. Failed with the error code ${response_json.error_msg}`);
            }
        } else {
            return response_json.data;
        }

    }
}





