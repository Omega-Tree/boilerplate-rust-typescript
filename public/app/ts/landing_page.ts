
import {APIClient} from './api_client';

window.onload = function () {
    let body = document.querySelector('body');
    
    if (body) {
        let api_client = new APIClient("http://127.0.0.1", 8081);
            
        let login_container = document.createElement('div');
        let login_title     = document.createElement('div');
        login_title.innerText = "Login";
        let username_container = document.createElement('div');
        let username_input = document.createElement('input');

        let password_container = document.createElement('div');
        let password_input = document.createElement('input');

        let login_btn_container = document.createElement('div');
        let login_btn = document.createElement('button');

        login_btn.innerHTML = "Login";


        let new_user_container = document.createElement('div');
        let new_user = document.createElement('a');
        
        new_user.innerHTML = "new user?";
        new_user.href = api_client.full_host + "/public/app/html/new_user.html";
        
        
        login_btn.addEventListener('click', () => {
            api_client.login("ttest", "4321tset");
        });
        
        password_input.setAttribute('type', 'password');
        
        body.appendChild(login_container);
        login_container.appendChild(login_title);
        login_container.appendChild(username_container);
        username_container.appendChild(username_input);
        login_container.appendChild(password_container);
        password_container.appendChild(password_input);
        new_user_container.appendChild(new_user);
        login_container.appendChild(new_user_container);
        login_container.appendChild(login_btn_container);
        login_btn_container.appendChild(login_btn);



    } else {
        throw ("Could not find body tag!");
    }


}
